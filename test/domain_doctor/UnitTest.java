package domain_doctor;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class UnitTest {

    private Domain domain;

    @Before
    public void setUp() {
        domain = new Domain("hostica.com");
    }

    @After
    public void tearDown() {
        domain = null;
    }

    // Test for domain name
    @Test
    public void testDomainNameSetter() {
        String expected = "hostica.com";
        String actual = domain.getName();
        assertEquals("Domain object's name doesn't match!", expected, actual);
    }

    // Test for expiration date
    @Test
    public void testExpiryDate() {
        String expected = "2018-04-04";
        String actual = domain.getExpiryDate();
        assertEquals("Expiry Date doesn't match", expected, actual);

    }

    // Test check expiration date method
    @Test
    public void testCheckExpiration() {
        assertEquals("Domain is either expired or method" +
                "is not working", true, domain.checkExpirationDate());
    }
}
