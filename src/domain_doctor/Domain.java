package domain_doctor;

import org.apache.commons.net.whois.WhoisClient;

import java.io.IOException;
import java.time.LocalDate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Domain {
    private String name, expiryDate;
    private LocalDate today = LocalDate.now();

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getToday() {return this.today; }

    // Constructor
    public Domain(String name) {
        setName(name);
        setExpiration(name);
    }

    /** Whois lookup. Returns a string containing Whois output
     *
     * @param domainName
     * @return
     */
    private String whoisPerform(String domainName) {
        WhoisClient whois;
        String output;

        whois = new WhoisClient();

        try {
            whois.connect(WhoisClient.DEFAULT_HOST);
            output = whois.query(domainName);
            whois.disconnect();
            return output;
        } catch(IOException e) {
            System.err.println("Error I/O exception: " + e.getMessage());
            return null;
        }
    }


    /** Use regex to get the expiration date
     *
     * @param input
     * @return
     */
    private String extractExpiryDate(String input) {
        String output = "";
        Pattern expiryPattern = Pattern.compile("Expiry Date: \\d{4}-\\d{2}-\\d{2}");

        Matcher matcher = expiryPattern.matcher(input);
        matcher.find();
        output = matcher.group().replaceAll("Expiry Date: ", "");
        //output = matcher.toString();
        return output;
    }

    private void setExpiration(String domainName) {
        String whois = whoisPerform(domainName);
        String expiry = extractExpiryDate(whois);
        setExpiryDate(expiry);
    }

    // Checks expiration date vs current date to make sure domain is
    // not expired
    boolean checkExpirationDate() {
        LocalDate expire = LocalDate.parse(this.getExpiryDate());
        if(expire.toEpochDay() - this.getToday().toEpochDay() > 0) {
            return true;
        }
        else {
            return false;
        }
    }
}
