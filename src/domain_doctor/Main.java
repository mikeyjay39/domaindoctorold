package domain_doctor;
/*
To run from command line:

~/Hostica/domain_doctor/out/production/domain_doctor$ java -cp "domain_doctor/commons-net-3.6.jar:./" domain_doctor.Main hostica.com
https://stackoverflow.com/questions/219585/including-all-the-jars-in-a-directory-within-the-java-classpath
 */

import org.apache.commons.net.whois.WhoisClient;

import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        WhoisClient whois;
        String domain = args[0];

        whois = new WhoisClient();

        try {
            whois.connect(WhoisClient.DEFAULT_HOST);
            System.out.println(whois.query(domain));
            whois.disconnect();
        } catch(IOException e) {
            System.err.println("Error I/O exception: " + e.getMessage());
            return;
        }
    }
}
